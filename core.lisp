(defun pile (heure minute)
  (when (= minute 0)
      (format nil "il est pile ! (~2,'0d:~2,'0d)" heure minute)))

(defun reil (heure minute)
  (when (= heure minute)
    (format nil "il est ~2,'0d:~2,'0d !" heure minute)))
